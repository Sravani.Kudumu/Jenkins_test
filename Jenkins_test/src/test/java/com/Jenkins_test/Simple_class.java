package com.Jenkins_test;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class Simple_class {

	@Test
	public void Autospec() {
		ChromeOptions chromeOptions=new ChromeOptions();
		chromeOptions.setBinary("/opt/chromedriver");
	    ChromeDriver driver= new ChromeDriver(chromeOptions);
		driver.get("https://autospecs-test.pype.io");
		String title = driver.getTitle();
		System.out.println(title);
		driver.findElement(By.id("email")).sendKeys("sravani.kudumu@pype.io");
		Reporter.log("Entering Password..");
		driver.findElement(By.id("password")).sendKeys("Test1234");
		Reporter.log("Clickng on login..");
		driver.findElement(By.id("input[type='submit']")).click();
		System.out.println("Application login successfull");
	}
}
